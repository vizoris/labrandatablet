﻿jQuery(document).ready(function() {

// BEGIN of script for banner-slider
var bannerSlider = $('.banner-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		// autoplay: true,
		// autoplaySpeed: 3000,
		fade: true,
		pauseOnHover: false,
		speed: 1000,
		dots: true
	});
$('.banner-slider__next').click(function(){
 	$(bannerSlider).slick("slickNext");
 });
 $('.banner-slider__prev').click(function(){
 	$(bannerSlider).slick("slickPrev");
 });
// END of script for banner-slider



// FansyBox 2
$('.fancybox').fancybox();



// BEGIN of script for brands-slider
$('.brands-slider').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		arrows: true,
		// autoplay: true,
		// autoplaySpeed: 3000,
		pauseOnHover: false,
		speed: 1000,
		dots: false
	});

// END of script for brands-slider

// BEGIN of script for select-buyers Slider
$('.select-buyers__wrapper').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		arrows: true,
		// autoplay: true,
		// autoplaySpeed: 3000,
		pauseOnHover: false,
		speed: 1000,
		dots: false
	});

// END of script for select-buyers Slider

// BEGIN of script for product-slider__news
$('.product-slider__news').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: true,
		// autoplay: true,
		// autoplaySpeed: 3000,
		pauseOnHover: false,
		speed: 1000,
		dots: false,
	});

// END of script for product-slider__news

// BEGIN of script for product-slider__populars
$('.product-slider__populars').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		arrows: true,
		// autoplay: true,
		// autoplaySpeed: 3000,
		pauseOnHover: false,
		speed: 1000,
		dots: false,
	});

// END of script for product-slider__populars


// BEGIN of script for product-slider__sale
$('.product-slider__sale').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		arrows: true,
		// autoplay: true,
		// autoplaySpeed: 3000,
		pauseOnHover: false,
		speed: 1000,
		dots: false,
	});

// END of script for product-slider__sale

// BEGIN of script for .reviews-slider
$('.reviews-slider').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: true,
		// autoplay: true,
		// autoplaySpeed: 3000,
		pauseOnHover: false,
		speed: 1000,
		dots: false,
	});

// END of script for .reviews-slider


// Выпадающее меню футера
$('.footer-menu__drop > a').click(function() {
	$(this).toggleClass('active');
	$(this).next('ul').fadeToggle();
})

// Фильтр
$('.filter-btn__catalog').click(function() {
	$('.sidebar').toggleClass('active');
})
$('.filter-btn__sidebar').click(function() {
	$('.sidebar').toggleClass('active');
})



// Слайдер страницы товара
var productPageSlider =  $('.product-page__slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
    });

// Скрипт главного МЕНЮ сайта
$('.header-menu, .main-menu__close').click(function() {
	$('.main-menu__overlay').toggleClass('active');
	$('.main-menu__wrapper').toggleClass('active');
})






// Phone mask
$("#phone-mask").mask("+7 (999) 99-99-999");


// Кастомный скролл
$(function() {
    $('.scroll-box').jScrollPane({
			verticalDragMinHeight: 10,
			verticalDragMaxHeight: 10,
			horizontalDragMinWidth: 10,
			horizontalDragMaxWidth: 10
		});
});

$(function() {
    $('.sidebar-scroll').jScrollPane({
			verticalDragMinHeight: 25,
			verticalDragMaxHeight: 25,
			horizontalDragMinWidth: 25,
			horizontalDragMaxWidth: 25
		});
});

// BEGIN of script for header submenu
	$(".navbar-toggle").on("click", function () {
		$(this).toggleClass("active");
	});
// END of script for header submenu

// BEGIN of script to stick header
	$(window).scroll(function(){
		var sticky = $('header'),
			scroll = $(window).scrollTop();
		if (scroll > 60) {
			sticky.addClass('header-fixed');
		} else {
			sticky.removeClass('header-fixed');
		};
	});
// END of script to stick header


// BEGIN of script for full-slider
var fullSlider = $('.full-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		// autoplay: true,
		// autoplaySpeed: 3000,
		fade: true,
		pauseOnHover: false,
		speed: 1000,
		dots: true
	});
$('.full-slider__next').click(function(){
 	$(fullSlider).slick("slickNext");
 });
 $('.full-slider__prev').click(function(){
 	$(fullSlider).slick("slickPrev");
 });
// END of script for banner-slider

// Begin of slider-range
$( "#slider-range" ).slider({
	range: true,
	min: 500,
	max: 15000,
	values: [ 0, 9600 ],
	slide: function( event, ui ) {
		$( "#amount" ).val( ui.values[ 0 ] );
		$( "#amount-1" ).val( ui.values[ 1 ] );
	}
});
$( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 ) );
$( "#amount-1" ).val( $( "#slider-range" ).slider( "values", 1 ) );

      // Изменение местоположения ползунка при вводиде данных в первый элемент input
      $("input#amount").change(function(){
      	var value1=$("input#amount").val();
      	var value2=$("input#amount-1").val();
      	if(parseInt(value1) > parseInt(value2)){
      		value1 = value2;
      		$("input#amount").val(value1);
      	}
      	$("#slider-range").slider("values",0,value1); 
      });
      
      // Изменение местоположения ползунка при вводиде данных в второй элемент input  
      $("input#amount-1").change(function(){
      	var value1=$("input#amount").val();
      	var value2=$("input#amount-1").val();

      	if(parseInt(value1) > parseInt(value2)){
      		value2 = value1;
      		$("input#amount-1").val(value2);
      	}
      	$("#slider-range").slider("values",1,value2);
      });
// END of slider-range

// Показать количество товаров
$('.check-list input[type="checkbox"]').click(function() {
if ($(this).is(':checked')) {
	$(this).parents(".show-product__visible").children(".show-product").addClass("active");
}else{
	$(this).parents(".show-product__visible").children(".show-product").removeClass("active");
}
})

// Сгорнуть фильтр
$(".filter-item__cut").click(function() {
	$(this).toggleClass('active');
	$(this).next().fadeToggle();
});


// Сгорнуть ВСЕ фильтры
$(".show-hide").click(function() {
	$(this).toggleClass('active');
	$(".filter-item").fadeToggle();
})


// Забыли пароль

$(".input-wrapper input").focus(function() {
	$(this).next().addClass("active");

});

$(".input-wrapper input").blur(function() {
    if ($(this).val()== "") {
    	$(this).next().removeClass("active");
    }
});





});


